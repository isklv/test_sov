(function() {
    var cookie_name = "scb_utmz";
    var url = document.location.href;
    var path = "/";
    var domain = "sovcombank.ru";
    var cookie_value = "";
    //182 дня 24 часа 60 минут и 60 секунд
    var duration = 182 * 24 * 60 * 60; // полгода как и __utmz

    //функцию лучше объявлять так (Защита от поднятия переменных)
    function setCookie(sKey, sValue, vEnd, sPath, sDomain, bSecure) {
        ///^(?:expire|max\-age|path|domain|secure)$/i.test(sKey) не понимаю зачем здесь эта проверка? Содержит ли sKey expire|max-age|path|domain|secure
        if (!sKey || /^(?:expire|max\-age|path|domain|secure)$/i.test(sKey)) {
            return false;
        }
        var sExpires = "";
        if (vEnd) {
            switch (vEnd.constructor) {
                case Number:
                    sExpires = vEnd === Infinity ? "; expires=Fri, 31 Dec 9999 23:59:59 GMT" : "; max-age=" + vEnd;
                    break;
                case String:
                    //переводим строку в дату
                    sExpires = "; expires=" + (new Date(vEnd)).toUTCString();
                    break;
                case Date:
                    sExpires = "; expires=" + vEnd.toUTCString();
                    break;
            }
        }
        document.cookie = encodeURIComponent(sKey) + "=" +
            encodeURIComponent(sValue) + sExpires + (sDomain ? "; domain=" + sDomain : "") +
            (sPath ? "; path=" + sPath : "") + (bSecure ? "; secure" : "");

        return true;
    }

    function getCookie(cookieName){
        // var name = cookieName + "=";
        // var cookieArray = document.cookie.split(';');
        // for(var i = 0; i < cookieArray.length; i++){
        //     var cookie = cookieArray[i].replace(/^\s+|\s+$/g, '');
        //     if (cookie.indexOf(name) === 0){
        //         return cookie.substring(name.length,cookie.length);
        //     }
        // }
        // return null;
        // я бы так переписал. Ищет cookieName с помощью регулярки и не перебирает массив
        var matches = document.cookie.match(new RegExp(
            "(?:^|; )" + cookieName.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : null;
    }

    function getUrlParameter(sParam) {
        //здесь тоже можно посадить на регулярку и в старом коде не было decodeURIComponent. Символы неправильно бы показывались
        var sPageURL = window.location.search,
            regex = new RegExp("[?&]" + sParam + "(=([^&#]*)|&|#|$)"),
            result = regex.exec(sPageURL);

        if(!result) return "(not set)";
        return decodeURIComponent(result[2].replace(/\+/g, " "));
        // var sPageURL = window.location.search.substring(1),
        //     sURLVariables = sPageURL.split('&');
        // for (var i = 0; i < sURLVariables.length; i++)  {
        //     var sParameterName = sURLVariables[i].split('=');
        //     if (sParameterName[0] == sParam)  {
        //         return sParameterName[1];
        //     }
        // }
        //return "(not set)";
    }

    var organic_sources = [
        {
            pattern: "google",
            key: "q"
        },
        {
            pattern: "nova.rambler.ru",
            key: "query"
        },
        {
            pattern: "yahoo.com",
            key: "p"
        },
        {
            pattern: "aport.ru",
            key: "r"
        },
        {
            pattern: "go.mail.ru",
            key: "q"
        },
        {
            pattern: "nigma.ru",
            key: "s"
        },
        {
            pattern: "webalta.ru",
            key: "q"
        },
        {
            pattern: "aport.ru",
            key: "r"
        },
        {
            pattern: "poisk.ru",
            key: "text"
        },
        {
            pattern: "km.ru",
            key: "sq"
        },
        {
            pattern: "liveinternet.ru",
            key: "q"
        },
        {
            pattern: "quintura.ru",
            key: "request"
        },
        {
            pattern: "search.qip.ru",
            key: "query"
        },
        {
            pattern: "gde.ru",
            key: "keywords"
        },
        {
            pattern: "bing.com",
            key: "q"
        }
    ];

    function setSovcomCookie() {
        // лучше читается и рекомендуется объявлять так. Читаем get параметры
        var gclid = getUrlParameter('gclid'),
            source = getUrlParameter('utm_source'),
            medium = getUrlParameter('utm_medium'),
            campaign = getUrlParameter('utm_campaign'),
            keyword = getUrlParameter('utm_term'),
            content = getUrlParameter('utm_content'),
            cookie_value; // нет смысла перезаписывать глобальную (для нашего контекста) переменную. Потом можно запутаться.

        if (gclid !== "(not set)") { //если есть gclid
            cookie_value = "utmgclid=" + gclid + "|utmccn=(not set)|utmcmd=(not set)";
            setCookie(cookie_name, cookie_value, duration, path, domain);
        }
        else if (source !== "(not set)") {  //если есть utm_source
            cookie_value = "utmcsr=" + source + "|utmccn=" + campaign + "|utmcmd=" + medium + "|utmctr=" + keyword + "|utmcct=" + content;
            setCookie(cookie_name, cookie_value, duration, path, domain);
        }
        else if (document.referrer === "") { // если перешли сюда не по ссылке
            var c_value = getCookie(cookie_name);

            // была лишняя проверка и можно упростить
            cookie_value = (!c_value || c_value.indexOf("(direct)") > -1) ? "utmcsr=(direct)|utmccn=direct|utmcmd=Direct" : c_value ;

            setCookie(cookie_name, cookie_value, duration, path, domain);
        }
        else {
            var ref = document.referrer,
                isOrganic = false;
            // здесь определяем поисковик с которого пришли и текст запроса.

            for (var i = 0; i < organic_sources.length; i++) { //обходим поисковики
                var s = organic_sources[i];
                if (ref.search(s.pattern) > -1) { //соответствует ли

                    var keywordPattern = new RegExp(s.key + "=([^&]+)");
                    var match = keywordPattern.exec(ref); //ищем поисковый параметр

                    keyword = match && match.length == 2 ? match[1] : "(not set)"; // получаем поисковый параметр
                    //decodeURIComponent раскодируем символы
                    cookie_value = "utmcsr=" + s.pattern + "|utmccn=" + campaign + "|utmcmd=organic|utmctr=" + decodeURIComponent(keyword) + "|utmcct=" + content;
                    setCookie(cookie_name, cookie_value, duration, path, domain);

                    isOrganic = true;
                    break;
                }
            }

            // здесь в куки записывается адрес страницы, с которой к нам перешли. Не пишутся адреса organic_sources и https://sovcombank.ru
            if (false === isOrganic && /^https?:\/\/sovcombank\.ru/.exec(ref) === null) {
            	var arr_ref = ref.match(/^https?:\/\/(\w|\d|-|\.)*\.\w{2,24}/);
                cookie_value = "utmcsr=" + arr_ref[0] + "|utmccn=" + campaign + "|utmcmd=referral|utmctr=" + keyword + "|utmcct=" + content;
                setCookie(cookie_name, cookie_value, duration, path, domain);
            }
        }

        setCookie("__sovcom_sess", 1, 30 * 60, path, domain);
    }
    try {
        //убрал лишнюю проверку. Дублирует выполнение setSovcomCookie()
        setSovcomCookie();
    } catch (e) {
        //вывод для отладки
        console.log(e);
    }
})();
